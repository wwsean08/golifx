package golifx

import (
	"encoding/json"
	"net"
)

// MarshalJSON returns the JSON encoding of the Device
func (d Device) MarshalJSON() ([]byte, error) {
	temp := struct {
		HardwareAddress uint64               `json:"hardware_address"`
		IPAddress       net.Addr             `json:"ip_address"`
		Port            uint32               `json:"port"`
		Label           string               `json:"label"`
		PowerState      bool                 `json:"power_state"`
		InfraredLevel   uint16               `json:"infrared_level"`
		StateHostInfo   *DeviceSignalInfo    `json:"state_host_info"`
		WifiInfo        *DeviceSignalInfo    `json:"wifi_info"`
		Version         *DeviceVersion       `json:"version"`
		HostFirmware    *DeviceFirmware      `json:"host_firmware"`
		WifiFirmware    *DeviceFirmware      `json:"wifi_firmware"`
		Info            *DeviceStateInfo     `json:"info"`
		Location        *DeviceLocation      `json:"location"`
		Group           *DeviceLocation      `json:"group"`
		Color           *HSBK                `json:"color"`
		MultizoneInfo   *DeviceMultizoneInfo `json:"multizone_info"`
	}{
		HardwareAddress: d.hardwareAddress,
		IPAddress:       d.ipAddress,
		Port:            d.port,
		Label:           d.label,
		PowerState:      d.powerState,
		InfraredLevel:   d.infraredLevel,
		StateHostInfo:   d.stateHostInfo,
		WifiInfo:        d.wifiInfo,
		Version:         d.version,
		HostFirmware:    d.hostFirmware,
		WifiFirmware:    d.wifiFirmware,
		Info:            d.info,
		Location:        d.location,
		Group:           d.group,
		Color:           d.color,
		MultizoneInfo:   d.multizoneInfo,
	}
	return json.Marshal(temp)
}

// UnmarshalJSON parses the JSON-encoded data and stores the result in the Device.
func (d *Device) UnmarshalJSON(data []byte) error {
	temp := struct {
		HardwareAddress uint64               `json:"hardware_address"`
		IPAddress       net.Addr             `json:"ip_address"`
		Port            uint32               `json:"port"`
		Label           string               `json:"label"`
		PowerState      bool                 `json:"power_state"`
		InfraredLevel   uint16               `json:"infrared_level"`
		StateHostInfo   *DeviceSignalInfo    `json:"state_host_info"`
		WifiInfo        *DeviceSignalInfo    `json:"wifi_info"`
		Version         *DeviceVersion       `json:"version"`
		HostFirmware    *DeviceFirmware      `json:"host_firmware"`
		WifiFirmware    *DeviceFirmware      `json:"wifi_firmware"`
		Info            *DeviceStateInfo     `json:"info"`
		Location        *DeviceLocation      `json:"location"`
		Group           *DeviceLocation      `json:"group"`
		Color           *HSBK                `json:"color"`
		MultizoneInfo   *DeviceMultizoneInfo `json:"multizone_info"`
	}{}
	err := json.Unmarshal(data, &temp)
	if err != nil {
		return err
	}
	d.hardwareAddress = temp.HardwareAddress
	d.ipAddress = temp.IPAddress
	d.port = temp.Port
	d.label = temp.Label
	d.powerState = temp.PowerState
	d.infraredLevel = temp.InfraredLevel
	d.stateHostInfo = temp.StateHostInfo
	d.wifiInfo = temp.WifiInfo
	d.version = temp.Version
	d.hostFirmware = temp.HostFirmware
	d.wifiFirmware = temp.WifiFirmware
	d.info = temp.Info
	d.location = temp.Location
	d.group = temp.Group
	d.color = temp.Color
	d.multizoneInfo = temp.MultizoneInfo
	return nil
}
