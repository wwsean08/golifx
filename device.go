package golifx

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net"
	"strings"
	"time"
)

type (
	// Device represents one LIFX device on your network.
	Device struct {
		hardwareAddress   uint64
		ipAddress         net.Addr
		port              uint32
		label             string
		labelLastReceived time.Time
		powerState        bool
		infraredLevel     uint16
		stateHostInfo     *DeviceSignalInfo
		wifiInfo          *DeviceSignalInfo
		version           *DeviceVersion
		hostFirmware      *DeviceFirmware
		wifiFirmware      *DeviceFirmware
		info              *DeviceStateInfo
		location          *DeviceLocation
		group             *DeviceLocation
		color             *HSBK
		multizoneInfo     *DeviceMultizoneInfo
	}

	// DeviceSignalInfo represents the devices current signal and network IO of the Device.
	DeviceSignalInfo struct {
		lastReceived time.Time
		Signal       float32 `json:"signal"`
		Tx           uint32  `json:"tx"`
		Rx           uint32  `json:"rx"`
	}

	// DeviceVersion represents the version of the device as well as the DeviceCapabilities since that's derived.
	// from the version
	DeviceVersion struct {
		lastReceived time.Time
		Name         string `json:"name"`
		//nolint: golint // this is legacy and I don't feel like breaking the interface right now
		VendorId uint32 `json:"vendor_id"`
		//nolint: golint // this is legacy and I don't feel like breaking the interface right now
		ProductId    uint32              `json:"product_id"`
		Version      uint32              `json:"version"`
		Capabilities *DeviceCapabilities `json:"capabilities"`
	}

	// DeviceFirmware represents the version of the firmware.
	DeviceFirmware struct {
		lastReceived time.Time
		Build        uint64 `json:"build"`
		Version      uint32 `json:"version"`
	}

	// DeviceStateInfo represents the current state of the Device that may be interesting from a backend use case.
	DeviceStateInfo struct {
		lastReceived time.Time
		Time         time.Duration `json:"time"`
		UpTime       time.Duration `json:"up_time"`
		Downtime     time.Duration `json:"downtime"`
	}

	// DeviceLocation represents the location of the device as configured by the LIFX app.
	DeviceLocation struct {
		lastReceived time.Time
		Location     []byte        `json:"location"`
		Label        string        `json:"label"`
		UpdatedAt    time.Duration `json:"updated_at"`
	}

	// DeviceState represents the the devices current state that the user might worry about (color, power, label).
	DeviceState struct {
		Color *HSBK  `json:"color"`
		Power bool   `json:"power"`
		Label string `json:"label"`
	}

	// DeviceCapabilities represents the things that a Device is capable of doing according to LIFX.
	// See https://github.com/LIFX/products/blob/master/products.json
	DeviceCapabilities struct {
		Unknown          bool   `json:"unknown"`
		Color            bool   `json:"color"`
		Infrared         bool   `json:"infrared"`
		Matrix           bool   `json:"matrix"`
		Multizone        bool   `json:"multizone"`
		Chain            bool   `json:"chain"`
		TemperatureRange []uint `json:"temperature_range"`
		Relays           bool   `json:"relays"`
		Buttons          bool   `json:"buttons"`
		HEV              bool   `json:"hev"`
	}

	// DeviceMultizoneInfo represents the color settings of a multizone device like the Z-Strip or Beam.
	DeviceMultizoneInfo struct {
		lastReceived time.Time
		Count        uint16  `json:"count"`
		Index        uint16  `json:"index"`
		ColorsCount  uint8   `json:"colors_count"`
		Colors       []*HSBK `json:"colors"`
	}

	// RelayPowerState represents a single relay's current power state for a LIFX Switch.
	RelayPowerState struct {
		Index uint8 `json:"index"`
		Power bool  `json:"power"`
	}

	// MultiZoneEffectState represents a firmware effect for a MultiZone device.
	MultiZoneEffectState struct {
		InstanceID uint32 `json:"instance_id"`
		Type       uint8  `json:"type"`
		Speed      uint32 `json:"speed"`
		Duration   uint64 `json:"duration"`
		Parameters []byte `json:"parameters"`
	}

	// TileEffectState represents a firmware effect for a Tile device.
	TileEffectState struct {
		InstanceID   uint32  `json:"instance_id"`
		Type         uint8   `json:"type"`
		Speed        uint32  `json:"speed"`
		Duration     uint64  `json:"duration"`
		Parameters   []byte  `json:"parameters"`
		PaletteCount uint8   `json:"palette_count"`
		Palette      []*HSBK `json:"palette"`
	}

	// HevCycleState represents an Hev cycle.
	HevCycleState struct {
		Duration  uint32 `json:"duration"`
		Remaining uint32 `json:"remaining"`
		LastPower bool   `json:"last_power"`
	}

	// HevCycleConfiguration represents an Hev cycle configuration.
	HevCycleConfiguration struct {
		Indication bool   `json:"indication"`
		Duration   uint32 `json:"duration"`
	}
)

var (
	// ErrNoResponse is returned when an acknowledgement is not received
	ErrNoResponse = errors.New("no acknowledgement response")
	// ErrIncorrectResponseType is returned on receiving an unexpected response
	ErrIncorrectResponseType = errors.New("incorrect response type")
	// ErrEchoMaxRequest is returned when an echo requests data is too long
	ErrEchoMaxRequest = errors.New("echo request max length is 64")
	// ErrNotCapable is returned when a device doesn't have the capabilities for the api being called
	ErrNotCapable = errors.New("the device you are trying to manage does not support that api")
)

func (d *Device) sendAndReceive(msg *message) (*message, error) {
	return d.sendAndReceiveDead(msg, config.TTL)
}

func (d *Device) sendAndReceiveDead(msg *message, deadLine time.Duration) (*message, error) {
	msg.target = d.hardwareAddress
	messages, err := conn.sendAndReceiveDead(msg, deadLine, d.ipAddress, 1)

	if err != nil {
		return nil, err
	}

	for _, m := range messages {
		if m.target != d.hardwareAddress {
			continue
		}
		return m, nil
	}

	return nil, io.EOF
}

func (d *Device) sendWithAcknowledgement(msg *message, deadLine time.Duration) error {
	msg.target = d.hardwareAddress
	msg.ackRequired = true

	msg, err := d.sendAndReceiveDead(msg, deadLine)

	if err != nil {
		return err
	}

	if msg._type != acknowledgement {
		return ErrNoResponse
	}
	return nil
}

// MacAddress returns the string representation of the mac address of the Device.
func (d *Device) MacAddress() string {
	mac := make([]byte, 8)
	writeUInt64(mac, d.hardwareAddress)
	return strings.Replace(fmt.Sprintf("% x", mac[0:6]), " ", ":", -1)
}

// SetHardwareAddress is used to set the mac address of the Device.  This uint must be little endian.
func (d *Device) SetHardwareAddress(address uint64) {
	d.hardwareAddress = address
}

// IP returns the IP address of the device.  Note that if you manually created the device this may not be known.
func (d *Device) IP() net.Addr {
	return d.ipAddress
}

// GetPowerState gets the current power state from the device and returns that.
func (d *Device) GetPowerState() (bool, error) {
	msg, err := d.sendAndReceive(makeMessageWithType(getPower))

	if err != nil {
		return false, err
	}

	if msg._type != statePower {
		return false, ErrIncorrectResponseType
	}

	var state uint16

	readUint16(msg.payout, &state)

	d.powerState = state != 0
	return d.powerState, nil
}

// SetPowerState sets the power state of the device, true for on, false for off.
func (d *Device) SetPowerState(state bool) error {
	msg := makeMessageWithType(setPower)
	msg.payout = []byte{0, 0}

	if state {
		msg.payout = []byte{0xFF, 0xFF}
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)

	if err != nil {
		return err
	}

	d.powerState = state
	return nil
}

// GetRelayPowerState gets the power state from the specified index for devices that support it
func (d *Device) GetRelayPowerState(index uint8) (*RelayPowerState, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Relays {
			return nil, ErrNotCapable
		}
	}

	msg := makeMessageWithType(getRelayPowerState)
	msg.payout = []byte{index}
	resp, err := d.sendAndReceive(msg)
	if err != nil {
		return nil, err
	}
	if resp._type != stateRelayPowerState {
		return nil, ErrIncorrectResponseType
	}
	powerState := RelayPowerState{
		Index: resp.payout[0],
		Power: false,
	}
	status := new(uint16)
	readUint16(resp.payout[1:], status)
	if *status != uint16(0) {
		powerState.Power = true
	}
	return &powerState, nil

}

// SetRelayPowerState sets the power state for a specified index for devices that support it
func (d *Device) SetRelayPowerState(state bool, index uint8) error {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Relays {
			return ErrNotCapable
		}
	}
	msg := makeMessageWithType(setRelayPowerState)
	msg.payout = []byte{index}
	if state {
		msg.payout = append(msg.payout, 0xFF, 0xFF)
	} else {
		msg.payout = append(msg.payout, 0x00, 0x00)
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)
	return err
}

// GetLabel gets the current label from the device.
func (d *Device) GetLabel() (string, error) {
	if time.Now().Before(d.labelLastReceived.Add(config.CacheTTL)) {
		return d.label, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getLabel))

	if err != nil {
		return "", err
	}

	if msg._type != stateLabel {
		return "", ErrIncorrectResponseType
	}

	d.label = string(bytes.Trim(msg.payout, "\x00"))
	d.labelLastReceived = time.Now()

	return d.label, nil
}

// SetLabel allows you to set the label on the device.  The max length of the string you can send is 32 bytes,
// anything else will be chopped off.
func (d *Device) SetLabel(label string) error {
	msg := makeMessageWithType(setLabel)

	msg.payout = []byte(label)

	if len(msg.payout) > 32 {
		msg.payout = msg.payout[:32]
	} else {
		msg.payout = append(msg.payout, make([]byte, 32-len(msg.payout))...)
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)

	if err != nil {
		return err
	}

	d.label = label
	d.labelLastReceived = time.Now()
	return nil
}

// GetStateHostInfo gets the current state host info from the device.
func (d *Device) GetStateHostInfo() (*DeviceSignalInfo, error) {
	if d.stateHostInfo != nil && time.Now().Before(d.stateHostInfo.lastReceived.Add(config.CacheTTL)) {
		return d.stateHostInfo, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getHostInfo))

	if err != nil {
		return nil, err
	}

	if msg._type != stateHostInfo {
		return nil, ErrIncorrectResponseType
	}

	d.stateHostInfo = parseSignal(msg.payout)
	d.stateHostInfo.lastReceived = time.Now()
	return d.stateHostInfo, nil
}

// GetWifiInfo gets the current DeviceSignalInfo from the device.
func (d *Device) GetWifiInfo() (*DeviceSignalInfo, error) {
	if d.wifiInfo != nil && time.Now().Before(d.wifiInfo.lastReceived.Add(config.CacheTTL)) {
		return d.wifiInfo, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getWifiInfo))

	if err != nil {
		return nil, err
	}

	if msg._type != stateWifiInfo {
		return nil, ErrIncorrectResponseType
	}

	d.wifiInfo = parseSignal(msg.payout)
	d.wifiInfo.lastReceived = time.Now()
	return d.wifiInfo, nil
}

// GetVersion gets the current DeviceVersionj from the device.
func (d *Device) GetVersion() (*DeviceVersion, error) {
	if d.version != nil && time.Now().Before(d.version.lastReceived.Add(config.CacheTTL)) {
		return d.version, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getVersion))

	if err != nil {
		return nil, err
	}

	if msg._type != stateVersion {
		return nil, ErrIncorrectResponseType
	}

	d.version = &DeviceVersion{}

	readUint32(msg.payout[:4], &d.version.VendorId)
	readUint32(msg.payout[4:8], &d.version.ProductId)
	readUint32(msg.payout[8:], &d.version.Version)

	d.version.Name = getNameByID(d.version.VendorId, d.version.ProductId)
	d.version.Capabilities = getCapabilitiesByID(d.version.VendorId, d.version.ProductId)
	d.version.lastReceived = time.Now()
	return d.version, nil
}

// GetHostFirmware gets the current DeviceFirmware of the device.
func (d *Device) GetHostFirmware() (*DeviceFirmware, error) {
	if d.hostFirmware != nil && time.Now().Before(d.hostFirmware.lastReceived.Add(config.CacheTTL)) {
		return d.hostFirmware, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getHostFirmware))

	if err != nil {
		return nil, err
	}

	if msg._type != stateHostFirmware {
		return nil, ErrIncorrectResponseType
	}

	d.hostFirmware = parseFirmware(msg.payout)
	d.hostFirmware.lastReceived = time.Now()
	return d.hostFirmware, nil
}

// GetWifiFirmware gets the current DeviceFirmware for the Wifi adapter of the Device.
func (d *Device) GetWifiFirmware() (*DeviceFirmware, error) {
	if d.wifiFirmware != nil && time.Now().Before(d.wifiFirmware.lastReceived.Add(config.CacheTTL)) {
		return d.wifiFirmware, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getWifiFirmware))

	if err != nil {
		return nil, err
	}

	if msg._type != stateWifiFirmware {
		return nil, ErrIncorrectResponseType
	}

	d.wifiFirmware = parseFirmware(msg.payout)
	d.wifiFirmware.lastReceived = time.Now()

	return d.wifiFirmware, nil
}

// GetInfo gets the current DeviceStateInfo of the device.
func (d *Device) GetInfo() (*DeviceStateInfo, error) {
	if d.info != nil && time.Now().Before(d.info.lastReceived.Add(config.CacheTTL)) {
		return d.info, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getInfo))

	if err != nil {
		return nil, err
	}

	if msg._type != stateInfo {
		return nil, ErrIncorrectResponseType
	}

	d.info = &DeviceStateInfo{}

	var i uint64

	readUint64(msg.payout[:8], &i)
	d.info.Time = time.Duration(i)
	readUint64(msg.payout[8:16], &i)
	d.info.UpTime = time.Duration(i)
	readUint64(msg.payout[16:], &i)
	d.info.Downtime = time.Duration(i)
	d.info.lastReceived = time.Now()
	return d.info, nil
}

// GetLocation gets the DeviceLocation that has been set for the Device.
func (d *Device) GetLocation() (*DeviceLocation, error) {
	if d.location != nil && time.Now().Before(d.location.lastReceived.Add(config.CacheTTL)) {
		return d.location, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getLocation))

	if err != nil {
		return nil, err
	}

	if msg._type != stateLocation {
		return nil, ErrIncorrectResponseType
	}

	d.location = parseLocation(msg.payout)
	d.location.lastReceived = time.Now()
	return d.location, nil
}

// GetGroup gets the DeviceLocation that has been set for the group of the Device
// (under the covers locations/groups are represented the same way).
func (d *Device) GetGroup() (*DeviceLocation, error) {
	if d.group != nil && time.Now().Before(d.group.lastReceived.Add(config.CacheTTL)) {
		return d.group, nil
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getGroup))

	if err != nil {
		return nil, err
	}

	if msg._type != stateGroup {
		return nil, ErrIncorrectResponseType
	}

	d.group = parseLocation(msg.payout)
	d.group.lastReceived = time.Now()
	return d.group, nil
}

// EchoRequest sends a message to the Device to respond with as an "echo".
func (d *Device) EchoRequest(request []byte) ([]byte, error) {
	if len(request) > 64 {
		return nil, ErrEchoMaxRequest
	}

	msg := makeMessageWithType(echoRequest)
	msg.payout = request
	msg, err := d.sendAndReceive(msg)

	if err != nil {
		return nil, err
	}

	if msg._type != echoResponse {
		return nil, ErrIncorrectResponseType
	}

	return msg.payout, nil
}

// GetPowerDurationState Gets the current power state, true for on, false for off.
func (d *Device) GetPowerDurationState() (bool, error) {
	msg, err := d.sendAndReceive(makeMessageWithType(getPowerDuration))

	if err != nil {
		return false, err
	}

	if msg._type != powerStateDuration {
		return false, ErrIncorrectResponseType
	}

	var state uint16

	readUint16(msg.payout, &state)

	d.powerState = state != 0
	return d.powerState, nil
}

// SetPowerDurationState sets the current power state, true for on, and false for off, with a transition over
// duration milliseconds.
func (d *Device) SetPowerDurationState(state bool, duration uint32) error {
	msg := makeMessageWithType(setPowerDuration)

	msg.payout = make([]byte, 6)

	if state {
		msg.payout[0], msg.payout[1] = 0xFF, 0xFF
	}

	if duration > 0 {
		writeUInt32(msg.payout[2:], duration)
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)

	if err != nil {
		return err
	}

	d.powerState = state
	return nil
}

// GetColorState gets the current DeviceState of the device.
func (d *Device) GetColorState() (*DeviceState, error) {
	msg, err := d.sendAndReceive(makeMessageWithType(get))

	if err != nil {
		return nil, err
	}

	if msg._type != state {
		return nil, ErrIncorrectResponseType
	}

	state := parseColorState(msg.payout)
	d.powerState = state.Power
	d.label = state.Label
	d.color = state.Color
	return state, nil
}

// SetColorState sets the current color of the device with a transition time of duration in milliseconds.
func (d *Device) SetColorState(hsbk *HSBK, duration uint32) error {
	msg := makeMessageWithType(setColor)
	msg.payout = make([]byte, 13)

	hsbk.Read(msg.payout[1:9])

	if duration > 0 {
		writeUInt32(msg.payout[9:], duration)
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)

	if err != nil {
		return err
	}

	d.color = hsbk

	return nil
}

// SetColorStateWithResponse sets the current color of the device with a transition time of duration in milliseconds,
// and returns the new DeviceState.
func (d *Device) SetColorStateWithResponse(hsbk *HSBK, duration uint32) (*DeviceState, error) {
	msg := makeMessageWithType(setColor)
	msg.resRequired = true
	msg.payout = make([]byte, 13)

	hsbk.Read(msg.payout[1:9])

	if duration > 0 {
		writeUInt32(msg.payout[9:], duration)
	}

	msg, err := d.sendAndReceive(msg)

	if err != nil {
		return nil, err
	}

	state := parseColorState(msg.payout)
	d.powerState = state.Power
	d.label = state.Label
	d.color = state.Color
	return state, nil
}

//SetWaveform Sets the waveform based on https://lan.developer.lifx.com/docs/waveforms returning the new DeviceState.
func (d *Device) SetWaveform(transient bool, hsbk *HSBK, period uint32, cycles float32, skewRatio int16, waveform uint8) (*DeviceState, error) {
	msg := makeMessageWithType(setWaveform)
	msg.resRequired = true
	msg.payout = make([]byte, 21)

	msg.payout[1] = boolToUInt8(transient)
	hsbk.Read(msg.payout[2:10])
	writeUInt32(msg.payout[10:], period)
	writeFloat32(msg.payout[14:], cycles)
	writeInt16(msg.payout[18:], skewRatio)
	msg.payout[20] = waveform

	msg, err := d.sendAndReceive(msg)

	if err != nil {
		return nil, err
	}

	state := parseColorState(msg.payout)
	d.powerState = state.Power
	d.label = state.Label
	d.color = state.Color
	return state, nil
}

// GetInfraredLevel should get the current infrared level of a device, but has not been tested due to lack of owning a
// device, if you run into issues, please let me know
func (d *Device) GetInfraredLevel() (uint16, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Infrared {
			return 0, ErrNotCapable
		}
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getInfrared))

	if err != nil {
		return 0, err
	}

	if msg._type != stateInfrared {
		return 0, ErrIncorrectResponseType
	}

	var state uint16

	readUint16(msg.payout, &state)

	d.infraredLevel = state
	return d.infraredLevel, nil
}

// SetInfraredLevel should set the infrared level of a device, but has not been tested due to lack of owning a device,
// if you run into issues, please let me know.
func (d *Device) SetInfraredLevel(level uint16) error {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Infrared {
			return ErrNotCapable
		}
	}
	msg := makeMessageWithType(setInfrared)

	msg.payout = make([]byte, 2)
	writeUInt16(msg.payout, level)

	err := d.sendWithAcknowledgement(msg, config.TTL)

	if err != nil {
		return err
	}

	d.infraredLevel = level
	return nil
}

// GetHevCycle gets information on the current Hev Cycle.  See, https://lan.developer.lifx.com/docs/querying-the-device-for-data#gethevcycle---packet-142
func (d *Device) GetHevCycle() (*HevCycleState, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.HEV {
			return nil, ErrNotCapable
		}
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getHevCycle))
	if err != nil {
		return nil, err
	}
	if msg._type != stateHevCycle {
		return nil, ErrIncorrectResponseType
	}
	hevCycle := new(HevCycleState)
	readUint32(msg.payout[0:4], &hevCycle.Duration)
	readUint32(msg.payout[4:8], &hevCycle.Remaining)
	hevCycle.LastPower = false
	if msg.payout[8] == 1 {
		hevCycle.LastPower = true
	}

	return hevCycle, nil
}

// SetHevCycle sets the current Hev cycle.  See https://lan.developer.lifx.com/docs/changing-a-device#sethevcycle---packet-143
func (d *Device) SetHevCycle(enabled bool, duration uint32) error {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.HEV {
			return ErrNotCapable
		}
	}

	msg := makeMessageWithType(setHevCycle)
	payload := make([]byte, 5)

	if enabled {
		payload[0] = 1
	} else {
		payload[0] = 0
	}
	writeUInt32(payload[1:5], duration)

	msg.payout = payload
	return d.sendWithAcknowledgement(msg, config.TTL)
}

// GetHevCycleConfiguration Get the current Hev Cycle Configuration.  See https://lan.developer.lifx.com/docs/querying-the-device-for-data#gethevcycleconfiguration---packet-145
func (d *Device) GetHevCycleConfiguration() (*HevCycleConfiguration, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.HEV {
			return nil, ErrNotCapable
		}
	}

	msg, err := d.sendAndReceive(makeMessageWithType(getHevCycleConfig))
	if err != nil {
		return nil, err
	}
	if msg._type != stateHevCycleConfig {
		return nil, ErrIncorrectResponseType
	}

	hevCycleConfig := new(HevCycleConfiguration)
	if msg.payout[0] == 1 {
		hevCycleConfig.Indication = true
	} else {
		hevCycleConfig.Indication = false
	}
	readUint32(msg.payout[1:5], &hevCycleConfig.Duration)

	return hevCycleConfig, nil
}

// SetHevCycleConfiguration Set the Hev Cycle Configuration.  See https://lan.developer.lifx.com/docs/changing-a-device#sethevcycleconfiguration---packet-146
func (d *Device) SetHevCycleConfiguration(indication bool, duration uint32) error {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.HEV {
			return ErrNotCapable
		}
	}
	msg := makeMessageWithType(setHevCycleConfig)
	payload := make([]byte, 5)
	if indication {
		payload[0] = 1
	} else {
		payload[0] = 0
	}
	writeUInt32(payload[1:5], duration)

	msg.payout = payload
	return d.sendWithAcknowledgement(msg, config.TTL)
}

// GetLastHevCycleResults Gets the last Hev cycle results.  See https://lan.developer.lifx.com/docs/querying-the-device-for-data#getlasthevcycleresult---packet-148
func (d *Device) GetLastHevCycleResults() (uint8, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.HEV {
			return HevCycleResultNone, ErrNotCapable
		}
	}
	msg, err := d.sendAndReceive(makeMessageWithType(getLastHevCycleResult))
	if err != nil {
		return HevCycleResultNone, err
	}
	if msg._type != stateLastHevCycleResult {
		return HevCycleResultNone, ErrIncorrectResponseType
	}

	return msg.payout[0], nil
}

// GetExtendedColorZones gets the current DeviceMultizoneInfo of the device if it has the multizone capability or
//capabilities are unknown (in this case the device may not respond).
func (d *Device) GetExtendedColorZones() (*DeviceMultizoneInfo, error) {
	if d.multizoneInfo != nil && time.Now().Before(d.multizoneInfo.lastReceived.Add(config.CacheTTL)) {
		return d.multizoneInfo, nil
	}
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Multizone {
			return nil, ErrNotCapable
		}
	}

	msg, err := d.sendAndReceive(makeMessageWithType(getExtendedColorZones))
	if err != nil {
		return nil, err
	}

	if msg._type != stateExtendedColorZones {
		return nil, ErrIncorrectResponseType
	}

	mzInfo := new(DeviceMultizoneInfo)
	count := new(uint16)
	index := new(uint16)
	readUint16(msg.payout[:2], count)
	readUint16(msg.payout[2:4], index)
	mzInfo.Count = *count
	mzInfo.Index = *index
	mzInfo.ColorsCount = msg.payout[4]

	colors := make([]*HSBK, int(mzInfo.ColorsCount))
	offset := 5
	for i := 0; i < int(mzInfo.ColorsCount); i++ {
		hsbkTemp := new(HSBK)
		hsbkTemp.Write(msg.payout[offset : offset+8])
		offset += 8
		colors[i] = hsbkTemp
	}
	mzInfo.Colors = colors
	d.multizoneInfo = mzInfo
	d.multizoneInfo.lastReceived = time.Now()
	return mzInfo, nil
}

// SetExtendedColorZones Sets the color zones of the device if it supports the multizone capability or it's capabilities
//are unknown (may cause issues, untested on devices that don't support it).
//
// color is a slice of how each zone should be set.
// duration is the transition time in milliseconds.
// index is the starting index of change on the device.
// apply is described by https://lan.developer.lifx.com/docs/multizone-messages#section-application-request.
func (d *Device) SetExtendedColorZones(color []*HSBK, duration uint32, index uint16, apply uint8) error {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Multizone {
			return ErrNotCapable
		}
	}

	colorCount := uint8(len(color))
	msg := makeMessageWithType(setExtendedColorZones)
	msg.payout = make([]byte, 664)

	if len(color) < 82 {
		tmp := HSBK{
			Hue:        0,
			Saturation: 0,
			Brightness: 0,
			Kelvin:     5000,
		}
		for i := len(color); i < 82; i++ {
			color = append(color, &tmp)
		}
	} else if len(color) > 82 {
		color = color[:82]
		colorCount = 82
	}

	writeUInt32(msg.payout[:4], duration)
	msg.payout[4] = apply
	writeUInt16(msg.payout[5:7], index)
	msg.payout[7] = colorCount

	offset := 8
	for i := 0; i < 82; i++ {
		color[i].Read(msg.payout[offset : offset+8])
		offset += 8
	}

	err := d.sendWithAcknowledgement(msg, config.TTL)
	if err != nil {
		return err
	}

	extendedColorZone := new(DeviceMultizoneInfo)
	extendedColorZone.Colors = color
	extendedColorZone.ColorsCount = colorCount
	extendedColorZone.Index = index
	d.multizoneInfo = extendedColorZone
	d.multizoneInfo.lastReceived = time.Now()
	return nil
}

// GetMultiZoneEffect is used to get the current firmware effect running on a device.  See https://lan.developer.lifx.com/docs/querying-the-device-for-data#getmultizoneeffect---packet-507
func (d *Device) GetMultiZoneEffect() (*MultiZoneEffectState, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Multizone {
			return nil, ErrNotCapable
		}
	}

	msg, err := d.sendAndReceive(makeMessageWithType(getMultiZoneEffect))
	if err != nil {
		return nil, err
	}
	if msg._type != stateMultiZoneEffect {
		return nil, ErrIncorrectResponseType
	}

	instanceID := new(uint32)
	speed := new(uint32)
	duration := new(uint64)
	readUint32(msg.payout[0:4], instanceID)
	_type := msg.payout[4]
	readUint32(msg.payout[5:9], speed)
	readUint64(msg.payout[9:17], duration)
	parameters := msg.payout[25:]

	state := MultiZoneEffectState{
		InstanceID: *instanceID,
		Type:       _type,
		Speed:      *speed,
		Duration:   *duration,
		Parameters: parameters,
	}

	return &state, nil
}

// SetMultiZoneMoveEffect is a convenience function for SetMultiZoneEffect using the move effect.  See https://lan.developer.lifx.com/docs/field-types#setmultizoneeffect---packet-508
func (d *Device) SetMultiZoneMoveEffect(instanceID uint32, speed uint32, duration uint64, direction uint32) error {
	parameters := make([]byte, 32)
	writeUInt32(parameters[4:8], direction)
	return d.SetMultiZoneEffect(instanceID, MultiZoneEffectMove, speed, duration, parameters)
}

// SetMultiZoneEffect allows you to start/stop firmware effects on MultiZone devices.  See https://lan.developer.lifx.com/docs/field-types#setmultizoneeffect---packet-508
func (d *Device) SetMultiZoneEffect(instanceID uint32, effect uint8, speed uint32, duration uint64, parameters []byte) error {
	if len(parameters) != 32 {
		return fmt.Errorf("invalid parameters for SetMultiZoneEffect, expected length of 32 bytes, but received %d bytes", len(parameters))
	}
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Multizone {
			return ErrNotCapable
		}
	}
	msg := makeMessageWithType(setMultiZoneEffect)
	msg.payout = make([]byte, 27)
	writeUInt32(msg.payout[0:4], instanceID)
	msg.payout[4] = effect
	writeUInt32(msg.payout[7:11], speed)
	writeUInt64(msg.payout[11:19], duration)
	msg.payout = append(msg.payout, parameters...)

	return d.sendWithAcknowledgement(msg, config.TTL)
}

// GetTileEffect is used to get the current state of firmware effects for tile devices.  See https://lan.developer.lifx.com/docs/changing-a-device#settileeffect---packet-719
func (d *Device) GetTileEffect() (*TileEffectState, error) {
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Matrix {
			return nil, ErrNotCapable
		}
	}

	msg, err := d.sendAndReceive(makeMessageWithType(getTileEffect))
	if err != nil {
		return nil, err
	}
	if msg._type != stateTileEffect {
		return nil, ErrIncorrectResponseType
	}

	instanceID := new(uint32)
	speed := new(uint32)
	duration := new(uint64)
	readUint32(msg.payout[1:5], instanceID)
	_type := msg.payout[5]
	readUint32(msg.payout[6:10], speed)
	readUint64(msg.payout[10:18], duration)
	parameters := msg.payout[18:50]
	paletteCount := msg.payout[50]
	palette := make([]*HSBK, 0, int(paletteCount))

	paletteSection := msg.payout[51:]
	offset := 0
	for i := 0; i < int(paletteCount); i++ {
		tmp := new(HSBK)
		tmp.Write(paletteSection[offset : offset+8])
		palette = append(palette, tmp)
		offset += 8
	}

	state := TileEffectState{
		InstanceID:   *instanceID,
		Type:         _type,
		Speed:        *speed,
		Duration:     *duration,
		Parameters:   parameters,
		PaletteCount: paletteCount,
		Palette:      palette,
	}

	return &state, nil
}

// SetTileEffect is used to set the firmware effect of tile devices.  See https://lan.developer.lifx.com/docs/changing-a-device#settileeffect---packet-719
func (d *Device) SetTileEffect(instanceID uint32, effect uint8, speed uint32, duration uint64, parameters []byte, paletteCount uint8, palette []*HSBK) error {
	if len(palette) > 16 {
		palette = palette[:16]
	} else if len(palette) < 16 {
		tmp := HSBK{
			Hue:        0,
			Saturation: 0,
			Brightness: 0,
			Kelvin:     5000,
		}
		for i := len(palette); i < 16; i++ {
			palette = append(palette, &tmp)
		}
	}
	if len(parameters) != 32 {
		return fmt.Errorf("invalid parameters for SetTileEffect, expected length of 32 bytes, but received %d bytes", len(parameters))
	}
	if d.version != nil && d.version.Capabilities != nil {
		if !d.version.Capabilities.Unknown && !d.version.Capabilities.Matrix {
			return ErrNotCapable
		}
	}

	msg := makeMessageWithType(setTileEffect)
	sliceLen := 60 + (8 * len(palette))
	data := make([]byte, sliceLen)
	writeUInt32(data[2:6], instanceID)
	data[6] = effect
	writeUInt32(data[7:11], speed)
	writeUInt64(data[11:19], duration)
	offset := 27
	for i, byt := range parameters {
		data[offset+i] = byt
	}
	data[59] = paletteCount
	offset = 60
	for _, hsbk := range palette {
		hsbk.Write(data[offset : offset+8])
		offset += 8
	}
	msg.payout = data

	return d.sendWithAcknowledgement(msg, config.TTL)
}

func parseSignal(payout []byte) *DeviceSignalInfo {
	signalInfo := &DeviceSignalInfo{}

	readFloat32(payout[:4], &signalInfo.Signal)
	readUint32(payout[4:8], &signalInfo.Tx)
	readUint32(payout[8:12], &signalInfo.Rx)
	return signalInfo
}

func parseFirmware(payout []byte) *DeviceFirmware {
	firmware := &DeviceFirmware{}

	readUint64(payout[:8], &firmware.Build)
	readUint32(payout[16:], &firmware.Version)

	return firmware
}

func parseLocation(payout []byte) *DeviceLocation {
	location := &DeviceLocation{}
	location.Location = payout[:16]
	location.Label = string(bytes.Trim(payout[16:48], "\x00"))
	var i uint64
	readUint64(payout[48:], &i)
	location.UpdatedAt = time.Duration(i)

	return location
}

func parseColorState(payout []byte) *DeviceState {
	hsbk := &HSBK{}
	hsbk.Write(payout[:8])

	state := &DeviceState{Color: hsbk}

	var powerState uint16
	readUint16(payout[10:12], &powerState)

	state.Power = powerState != 0

	state.Label = string(bytes.Trim(payout[12:44], "\x00"))
	return state
}
