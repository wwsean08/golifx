package golifx

import "time"

const (
	defaultMaxDeadLine = time.Millisecond * 500
	defaultPort        = 56700
	defaultBcast       = false
	defaultCacheTTL    = 0
)

// Config represents the config options used by the application, one is created with defaults at startup.
type Config struct {
	TTL             time.Duration
	CacheTTL        time.Duration
	Port            int
	AlwaysBroadcast bool
	OutboundIP      *string
}

var config *Config

func init() {
	config = new(Config)
	config.TTL = defaultMaxDeadLine
	config.Port = defaultPort
	config.AlwaysBroadcast = defaultBcast
	config.OutboundIP = nil
	config.CacheTTL = defaultCacheTTL
}

// SetTTL sets the TTL to listen for responses
func SetTTL(ttl time.Duration) {
	config.TTL = ttl
}

// SetPort sets the port to send messages on to LIFX devices
func SetPort(port int) {
	config.Port = port
}

// SetAlwaysBroadcast sets whether or not to broadcast even when the IP address is known
func SetAlwaysBroadcast(shouldBroadcast bool) {
	config.AlwaysBroadcast = shouldBroadcast
}

// SetOutboundIP sets the outbound IP address to use overriding the auto-discovery method.
func SetOutboundIP(ip *string) {
	config.OutboundIP = ip
}

// SetCacheTTL sets the TTL for cached data to be returned before it is refreshed on another call to the function.
func SetCacheTTL(ttl time.Duration) {
	config.CacheTTL = ttl
}

// SetConfig sets the config option being used by the LIFX library
func SetConfig(cfg *Config) {
	config = cfg
}
