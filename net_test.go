package golifx

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestNetConstantsDontChange(t *testing.T) {
	require.Equal(t, 56700, defaultPort)
	require.Equal(t, time.Duration(500000000), defaultMaxDeadLine)
}
