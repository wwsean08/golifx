package golifx

import (
	"encoding/binary"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestBoolToUint8(t *testing.T) {
	require.Equal(t, uint8(1), boolToUInt8(true))
	require.Equal(t, uint8(0), boolToUInt8(false))
}

func TestWriteUInt16(t *testing.T) {
	value := uint16(42)
	buffer := make([]byte, 2)
	writeUInt16(buffer, value)
	require.Equal(t, value, binary.LittleEndian.Uint16(buffer))
}

func TestWriteUInt32(t *testing.T) {
	value := uint32(42)
	buffer := make([]byte, 4)
	writeUInt32(buffer, value)
	require.Equal(t, value, binary.LittleEndian.Uint32(buffer))
}

func TestWriteUInt64(t *testing.T) {
	value := uint64(42)
	buffer := make([]byte, 8)
	writeUInt64(buffer, value)
	require.Equal(t, value, binary.LittleEndian.Uint64(buffer))
}

func TestReadUint16(t *testing.T) {
	buffer := make([]byte, 2)
	value := uint16(42)
	returnValue := uint16(0)
	binary.LittleEndian.PutUint16(buffer, value)
	readUint16(buffer, &returnValue)
	require.Equal(t, value, returnValue)
}

func TestReadUint32(t *testing.T) {
	buffer := make([]byte, 4)
	value := uint32(42)
	returnValue := uint32(0)
	binary.LittleEndian.PutUint32(buffer, value)
	readUint32(buffer, &returnValue)
	require.Equal(t, value, returnValue)
}

func TestReadUint64(t *testing.T) {
	buffer := make([]byte, 8)
	value := uint64(42)
	returnValue := uint64(0)
	binary.LittleEndian.PutUint64(buffer, value)
	readUint64(buffer, &returnValue)
	require.Equal(t, value, returnValue)
}

func TestMessageConstantsDontChange(t *testing.T) {
	require.Equal(t, 7, defaultSourceValue)
	require.Equal(t, 36, defaultHeaderLength)
}
