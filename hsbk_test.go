package golifx

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestHSBK_IsValid(t *testing.T) {
	tooLow := HSBK{
		Hue:        65535,
		Saturation: 65535,
		Brightness: 65535,
		Kelvin:     100,
	}

	err := tooLow.IsValid()
	require.Error(t, err)
	require.Equal(t, ErrInvalidHSBK, err)

	tooHigh := HSBK{
		Hue:        65535,
		Saturation: 65535,
		Brightness: 65535,
		Kelvin:     65535,
	}
	err = tooHigh.IsValid()
	require.Error(t, err)
	require.Equal(t, ErrInvalidHSBK, err)

	justRight := HSBK{
		Hue:        65535,
		Saturation: 65535,
		Brightness: 65535,
		Kelvin:     4200,
	}
	err = justRight.IsValid()
	require.NoError(t, err)
}

func TestHSBK_Write(t *testing.T) {
	hsbkData := HSBK{}
	data := []byte{
		255,
		255,
		255,
		255,
		255,
		255,
		104,
		16,
	}
	hsbkData.Write(data)

	require.Equal(t, uint16(65535), hsbkData.Hue)
	require.Equal(t, uint16(65535), hsbkData.Saturation)
	require.Equal(t, uint16(65535), hsbkData.Brightness)
	require.Equal(t, uint16(4200), hsbkData.Kelvin)
}

func TestHSBK_Read(t *testing.T) {
	hsbkData := HSBK{
		Hue:        65535,
		Saturation: 65535,
		Brightness: 65535,
		Kelvin:     4200,
	}
	data := make([]byte, 8)
	expectedData := []byte{
		255,
		255,
		255,
		255,
		255,
		255,
		104,
		16,
	}
	hsbkData.Read(data)
	require.Equal(t, expectedData, data)
}
