package main

import (
	"fmt"
	"gitlab.com/wwsean08/golifx"
	"time"
)

func main() {
	//Lookup all devices
	devices, _ := golifx.LookupDevices()
	//Get label
	label, _ := devices[0].GetLabel()

	fmt.Printf("Label: %s\n", label) //Ven LiFX

	//Get power state
	powerState, _ := devices[0].GetPowerState()

	//Turn if off
	if !powerState {
		devices[0].SetPowerState(true)
	}

	ticker := time.NewTicker(time.Second)
	counter := 0

	hsbk := &golifx.HSBK{
		Hue:        2000,
		Saturation: 13106,
		Brightness: 65535,
		Kelvin:     3200,
	}
	//Change color every second
	for range ticker.C {
		devices[0].SetColorState(hsbk, 500)
		counter++
		hsbk.Hue += 5000
		if counter > 10 {
			ticker.Stop()
			break
		}
	}
	//Turn off
	devices[0].SetPowerState(false)
}
