package golifx

import "errors"

// ErrInvalidHSBK is returned when the given HSBK object is invalid based on the LIFX specification.
var ErrInvalidHSBK = errors.New("invalid value for HSBK object")

// HSBK represents the color data sent/retrieved to your LIFX devices.
type HSBK struct {
	Hue        uint16 `json:"hue"`
	Saturation uint16 `json:"saturation"`
	Brightness uint16 `json:"brightness"`
	Kelvin     uint16 `json:"kelvin"`
}

// Write takes the data from an HSBK object and then writes it to a []byte of length 8.
func (h *HSBK) Write(data []byte) {
	readUint16(data[:2], &h.Hue)
	readUint16(data[2:4], &h.Saturation)
	readUint16(data[4:6], &h.Brightness)
	readUint16(data[6:8], &h.Kelvin)
}

// Read reads a []byte and stores it in the HSBK object.
func (h *HSBK) Read(p []byte) {
	data := make([]byte, 8)
	writeUInt16(data[:2], h.Hue)
	writeUInt16(data[2:4], h.Saturation)
	writeUInt16(data[4:6], h.Brightness)
	writeUInt16(data[6:8], h.Kelvin)

	copy(p, data)
}

// IsValid does some basic validation on an HSBK object to prevent causing issues.
func (h *HSBK) IsValid() error {
	if h.Kelvin < 1500 || h.Kelvin > 9000 {
		return ErrInvalidHSBK
	}
	return nil
}
