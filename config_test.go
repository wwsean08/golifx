package golifx

import (
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

func TestSetAlwaysBroadcast(t *testing.T) {
	require.False(t, config.AlwaysBroadcast)
	SetAlwaysBroadcast(true)
	require.True(t, config.AlwaysBroadcast)
	config.AlwaysBroadcast = defaultBcast
}

func TestSetOutboundIP(t *testing.T) {
	require.Nil(t, config.OutboundIP)
	ip := "192.168.1.1"
	SetOutboundIP(&ip)
	require.Equal(t, "192.168.1.1", *config.OutboundIP)
	config.OutboundIP = nil
}

func TestSetPort(t *testing.T) {
	require.Equal(t, 56700, config.Port)
	SetPort(443)
	require.Equal(t, 443, config.Port)
	config.Port = defaultPort
}

func TestSetTTL(t *testing.T) {
	require.Equal(t, time.Millisecond*500, config.TTL)
	SetTTL(time.Second)
	require.Equal(t, time.Second, config.TTL)
	config.TTL = defaultMaxDeadLine
}

func TestSetCacheTTL(t *testing.T) {
	require.Equal(t, time.Duration(0), config.CacheTTL)
	SetCacheTTL(time.Second)
	require.Equal(t, time.Second, config.CacheTTL)
	config.CacheTTL = defaultCacheTTL
}
