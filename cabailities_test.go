package golifx

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetNameByID(t *testing.T) {
	name := getNameByID(1, 43)
	require.Equal(t, "LIFX A19", name)

	name = getNameByID(9999, 9999)
	require.Equal(t, "Unknown", name)
}

func TestCapabilities(t *testing.T) {
	require := require.New(t)
	testCases := []struct {
		desc              string
		vendorID          uint32
		productID         uint32
		expectsColor      bool
		expectsInfrared   bool
		expectsMatrix     bool
		expectsMultizone  bool
		expectsChain      bool
		expectsHEV        bool
		expectsRelays     bool
		expectsButtons    bool
		expectsUnknown    bool
		expectedTempRange []uint
	}{
		{
			desc:              "Test device 1,45 has color and infrared",
			vendorID:          1,
			productID:         45,
			expectsColor:      true,
			expectsInfrared:   true,
			expectedTempRange: []uint{2500, 9000},
		},
		{
			desc:              "Test device 1,68 has the matrix capability",
			vendorID:          1,
			productID:         68,
			expectsMatrix:     true,
			expectedTempRange: []uint{1500, 9000},
		},
		{
			desc:              "Test device 1,55 has the chain capability",
			vendorID:          1,
			productID:         55,
			expectsChain:      true,
			expectsColor:      true,
			expectsMatrix:     true,
			expectedTempRange: []uint{2500, 9000},
		},
		{
			desc:              "Test device 1,38 has the multizone capability",
			vendorID:          1,
			productID:         38,
			expectsMultizone:  true,
			expectsColor:      true,
			expectedTempRange: []uint{2500, 9000},
		},
		{
			desc:           "Test device 9999,9999 has unknown capability",
			vendorID:       9999,
			productID:      9999,
			expectsUnknown: true,
		},
		{
			desc:           "Test device 1,9999 has unknown capability",
			vendorID:       1,
			productID:      9999,
			expectsUnknown: true,
		},
		{
			desc:           "Test device 1,70 has relays and button capability",
			vendorID:       1,
			productID:      70,
			expectsRelays:  true,
			expectsButtons: true,
		},
		{
			desc:              "Test device 1,90 has the HEV capabiliy",
			vendorID:          1,
			productID:         90,
			expectsHEV:        true,
			expectsColor:      true,
			expectedTempRange: []uint{2500, 9000},
		},
	}
	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			capabilities := getCapabilitiesByID(tC.vendorID, tC.productID)
			require.NotNil(capabilities)
			require.Equal(tC.expectsColor, capabilities.Color)
			require.Equal(tC.expectsInfrared, capabilities.Infrared)
			require.Equal(tC.expectsMatrix, capabilities.Matrix)
			require.Equal(tC.expectsMultizone, capabilities.Multizone)
			require.Equal(tC.expectsChain, capabilities.Chain)
			require.Equal(tC.expectsRelays, capabilities.Relays)
			require.Equal(tC.expectsButtons, capabilities.Buttons)
			require.Equal(tC.expectsHEV, capabilities.HEV)
			require.Equal(tC.expectedTempRange, capabilities.TemperatureRange)
		})
	}
}
