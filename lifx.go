package golifx

import (
	"fmt"
	"net"
)

var conn = &connection{bcastAddress: net.IPv4bcast}

const (
	getService        = 2
	stateService      = 3
	getHostInfo       = 12
	stateHostInfo     = 13
	getHostFirmware   = 14
	stateHostFirmware = 15
	getWifiInfo       = 16
	stateWifiInfo     = 17
	getWifiFirmware   = 18
	stateWifiFirmware = 19
	getPower          = 20
	setPower          = 21
	statePower        = 22
	getLabel          = 23
	setLabel          = 24
	stateLabel        = 25
	getVersion        = 32
	stateVersion      = 33
	getInfo           = 34
	stateInfo         = 35
	acknowledgement   = 45
	getLocation       = 48
	stateLocation     = 50
	getGroup          = 51
	stateGroup        = 53
	echoRequest       = 58
	echoResponse      = 59

	get                     = 101
	setColor                = 102
	setWaveform             = 103
	state                   = 107
	getPowerDuration        = 116
	setPowerDuration        = 117
	powerStateDuration      = 118
	getInfrared             = 120
	stateInfrared           = 121
	setInfrared             = 122
	getHevCycle             = 142
	setHevCycle             = 143
	stateHevCycle           = 144
	getHevCycleConfig       = 145
	setHevCycleConfig       = 146
	stateHevCycleConfig     = 147
	getLastHevCycleResult   = 148
	stateLastHevCycleResult = 149

	getMultiZoneEffect      = 507
	setMultiZoneEffect      = 508
	stateMultiZoneEffect    = 509
	setExtendedColorZones   = 510
	getExtendedColorZones   = 511
	stateExtendedColorZones = 512

	getTileEffect   = 718
	setTileEffect   = 719
	stateTileEffect = 720

	getRelayPowerState   = 816
	setRelayPowerState   = 817
	stateRelayPowerState = 818
)

const (
	// WaveformSaw Represents a saw waveform, see https://lan.developer.lifx.com/docs/waveforms
	WaveformSaw uint8 = 0
	// WaveformSine Represents a sine waveform, see https://lan.developer.lifx.com/docs/waveforms
	WaveformSine uint8 = 1
	// WaveformHalfSine Represents a half sine saw waveform, see https://lan.developer.lifx.com/docs/waveforms
	WaveformHalfSine uint8 = 2
	// WaveformTriangle Represents a triangle waveform, see https://lan.developer.lifx.com/docs/waveforms
	WaveformTriangle uint8 = 3
	// WaveformPulse Represents a pulse waveform, see https://lan.developer.lifx.com/docs/waveforms
	WaveformPulse uint8 = 4

	// NoApply represents the NO_APPLY option outlined at https://lan.developer.lifx.com/docs/multizone-messages#section-application-request
	NoApply uint8 = 0
	// Apply represents the APPLY option outlined at https://lan.developer.lifx.com/docs/multizone-messages#section-application-request
	Apply uint8 = 1
	// ApplyOnly represents the APPLY_ONLY option outlined at https://lan.developer.lifx.com/docs/multizone-messages#section-application-request
	ApplyOnly uint8 = 2

	// MultiZoneEffectOff is used to either turn off the MultiZone effect, or is returned when it is currently off.
	MultiZoneEffectOff uint8 = 0
	// MultiZoneEffectMove is used to turn on the "move" effect and is returned when it is currently on.
	MultiZoneEffectMove uint8 = 1

	// DirectionRight is used by the "move" MultiZone effect to make colors move right
	DirectionRight uint32 = 0
	// DirectionLeft is used by the "move" MultiZone effect to make colors move left
	DirectionLeft uint32 = 1

	// TileEffectOff is used to either turn off the firmware effect on tile devices, or is returned when it is currently off.
	TileEffectOff uint8 = 0
	// TileEffectMorph is used to turn on the "morph" firmware effect on tile devices, or is returned when it is currently on.
	TileEffectMorph uint8 = 2
	// TileEffectFlame is used to turn on the "flame" firmware effect on tile devices, or is returned when it is currently on.
	TileEffectFlame uint8 = 3

	// HevCycleResultSuccess Returned on a successful Hev cycle
	HevCycleResultSuccess uint8 = 0
	// HevCycleResultBusy returned on an ongoing Hev cycle
	HevCycleResultBusy uint8 = 1
	// HevCycleResultInterruptedByReset returned on a cycle that was interrupted by a reset
	HevCycleResultInterruptedByReset uint8 = 2
	// HevCycleResultInterruptedByHomeKit returned on a cycle that was interrupted by Home Kit
	HevCycleResultInterruptedByHomeKit uint8 = 3
	// HevCycleResultInterruptedByLAN returned on a cycle that was interrupted by a LAN call
	HevCycleResultInterruptedByLAN uint8 = 4
	// HevCycleResultInterruptedByCloud returned on a cycle that was interrupted by a Cloud call
	HevCycleResultInterruptedByCloud uint8 = 5
	// HevCycleResultNone returned when there are no results
	HevCycleResultNone uint8 = 255
)

// LookupDevices sends out a broadcast on your network to discover all currently connected LIFX devices.
func LookupDevices() ([]*Device, error) {
	message := makeMessage()
	message.tagged = true
	message._type = getService

	messages, err := conn.sendAndReceiveDead(message, config.TTL, nil, -1)

	if err != nil {
		return nil, err
	}

	devices := make([]*Device, 0)

	for _, message := range messages {
		if message.payout[0] != 1 {
			continue
		}

		device := &Device{}
		device.hardwareAddress = message.target
		device.ipAddress = message.addr

		var port uint32

		readUint32(message.payout[1:5], &port)
		device.port = port
		devices = append(devices, device)
	}

	return devices, nil
}

// LookupDeviceByIP will lookup a single device by it's IP address allowing for selective discovery,
//including discovery of devices that are not on the same network when communication between those networks
//is possible, but broadcast doesn't work.
func LookupDeviceByIP(deviceIP net.Addr) (*Device, error) {
	if deviceIP == nil {
		return nil, fmt.Errorf("device IP can't be nil when looking up device by IP")
	}

	message := makeMessage()
	message.tagged = true
	message._type = getService

	messages, err := conn.sendAndReceiveDead(message, config.TTL, deviceIP, 1)
	if err != nil {
		return nil, err
	}

	if len(messages) != 1 {
		return nil, fmt.Errorf("expected one message but found %d", len(messages))
	}

	device := &Device{}
	device.hardwareAddress = messages[0].target
	device.ipAddress = messages[0].addr

	var port uint32

	readUint32(messages[0].payout[1:5], &port)
	device.port = port

	return device, nil
}

// SetBroadcastAddress changes the address to use for broadcast messages, though there aren't any good reasons
// I can think of to use this.
func SetBroadcastAddress(addr net.IP) {
	conn.bcastAddress = addr
}
