package golifx

import (
	"github.com/stretchr/testify/require"
	"net"
	"sync"
	"testing"
)

func TestValidateConstsUnchanged(t *testing.T) {
	type testCase struct {
		constValue    int
		expectedValue int
		constName     string
	}
	testCases := []testCase{
		{
			constValue:    getService,
			expectedValue: 2,
			constName:     "getService",
		},
		{
			constValue:    stateService,
			expectedValue: 3,
			constName:     "stateService",
		},
		{
			constValue:    getHostInfo,
			expectedValue: 12,
			constName:     "getHostInfo",
		},
		{
			constValue:    stateHostInfo,
			expectedValue: 13,
			constName:     "stateHostInfo",
		},
		{
			constValue:    getHostFirmware,
			expectedValue: 14,
			constName:     "getHostFirmware",
		},
		{
			constValue:    stateHostFirmware,
			expectedValue: 15,
			constName:     "stateHostFirmware",
		},
		{
			constValue:    getWifiInfo,
			expectedValue: 16,
			constName:     "getWifiInfo",
		},
		{
			constValue:    stateWifiInfo,
			expectedValue: 17,
			constName:     "stateWifiInfo",
		},
		{
			constValue:    getWifiFirmware,
			expectedValue: 18,
			constName:     "getWifiFirmware",
		},
		{
			constValue:    stateWifiFirmware,
			expectedValue: 19,
			constName:     "stateWifiFirmware",
		},
		{
			constValue:    getPower,
			expectedValue: 20,
			constName:     "getPower",
		},
		{
			constValue:    setPower,
			expectedValue: 21,
			constName:     "setPower",
		},
		{
			constValue:    statePower,
			expectedValue: 22,
			constName:     "statePower",
		},
		{
			constValue:    getLabel,
			expectedValue: 23,
			constName:     "getLabel",
		},
		{
			constValue:    setLabel,
			expectedValue: 24,
			constName:     "setLabel",
		},
		{
			constValue:    stateLabel,
			expectedValue: 25,
			constName:     "stateLabel",
		},
		{
			constValue:    getVersion,
			expectedValue: 32,
			constName:     "getVersion",
		},
		{
			constValue:    stateVersion,
			expectedValue: 33,
			constName:     "stateVersion",
		},
		{
			constValue:    getInfo,
			expectedValue: 34,
			constName:     "getInfo",
		},
		{
			constValue:    stateInfo,
			expectedValue: 35,
			constName:     "stateInfo",
		},
		{
			constValue:    acknowledgement,
			expectedValue: 45,
			constName:     "acknowledgement",
		},
		{
			constValue:    getLocation,
			expectedValue: 48,
			constName:     "getLocation",
		},
		{
			constValue:    stateLocation,
			expectedValue: 50,
			constName:     "stateLocation",
		},
		{
			constValue:    getGroup,
			expectedValue: 51,
			constName:     "getGroup",
		},
		{
			constValue:    stateGroup,
			expectedValue: 53,
			constName:     "stateGroup",
		},
		{
			constValue:    echoRequest,
			expectedValue: 58,
			constName:     "echoRequest",
		},
		{
			constValue:    echoResponse,
			expectedValue: 59,
			constName:     "echoResponse",
		},
		{
			constValue:    get,
			expectedValue: 101,
			constName:     "get",
		},
		{
			constValue:    setColor,
			expectedValue: 102,
			constName:     "setColor",
		},
		{
			constValue:    setWaveform,
			expectedValue: 103,
			constName:     "setWaveform",
		},
		{
			constValue:    state,
			expectedValue: 107,
			constName:     "state",
		},
		{
			constValue:    getPowerDuration,
			expectedValue: 116,
			constName:     "getPowerDuration",
		},
		{
			constValue:    setPowerDuration,
			expectedValue: 117,
			constName:     "setPowerDuration",
		},
		{
			constValue:    powerStateDuration,
			expectedValue: 118,
			constName:     "powerStateDuration",
		},
		{
			constValue:    getInfrared,
			expectedValue: 120,
			constName:     "getInfrared",
		},
		{
			constValue:    stateInfrared,
			expectedValue: 121,
			constName:     "stateInfrared",
		},
		{
			constValue:    setInfrared,
			expectedValue: 122,
			constName:     "setInfrared",
		},
		{
			constValue:    getHevCycle,
			expectedValue: 142,
			constName:     "getHevCycle",
		},
		{
			constValue:    setHevCycle,
			expectedValue: 143,
			constName:     "setHevCycle",
		},
		{
			constValue:    stateHevCycle,
			expectedValue: 144,
			constName:     "stateHevCycle",
		},
		{
			constValue:    getHevCycleConfig,
			expectedValue: 145,
			constName:     "getHevCycleConfig",
		},
		{
			constValue:    setHevCycleConfig,
			expectedValue: 146,
			constName:     "setHevCycleConfig",
		},
		{
			constValue:    stateHevCycleConfig,
			expectedValue: 147,
			constName:     "stateHevCycleConfig",
		},
		{
			constValue:    getLastHevCycleResult,
			expectedValue: 148,
			constName:     "getLastHevCycleResult",
		},
		{
			constValue:    stateLastHevCycleResult,
			expectedValue: 149,
			constName:     "stateLastHevCycleResult",
		},
		{
			constValue:    getMultiZoneEffect,
			expectedValue: 507,
			constName:     "getMultiZoneEffect",
		},
		{
			constValue:    setMultiZoneEffect,
			expectedValue: 508,
			constName:     "setMultiZoneEffect",
		},
		{
			constValue:    stateMultiZoneEffect,
			expectedValue: 509,
			constName:     "stateMultiZoneEffect",
		},
		{
			constValue:    setExtendedColorZones,
			expectedValue: 510,
			constName:     "setExtendedColorZones",
		},
		{
			constValue:    getExtendedColorZones,
			expectedValue: 511,
			constName:     "getExtendedColorZones",
		},
		{
			constValue:    stateExtendedColorZones,
			expectedValue: 512,
			constName:     "stateExtendedColorZones",
		},
		{
			constValue:    getTileEffect,
			expectedValue: 718,
			constName:     "getTileEffect",
		},
		{
			constValue:    setTileEffect,
			expectedValue: 719,
			constName:     "setTileEffect",
		},
		{
			constValue:    stateTileEffect,
			expectedValue: 720,
			constName:     "stateTileEffect",
		},
		{
			constValue:    getRelayPowerState,
			expectedValue: 816,
			constName:     "getRelayPowerState",
		},
		{
			constValue:    setRelayPowerState,
			expectedValue: 817,
			constName:     "setRelayPowerState",
		},
		{
			constValue:    stateRelayPowerState,
			expectedValue: 818,
			constName:     "stateRelayPowerState",
		},
	}

	for _, test := range testCases {
		require.Equal(t, test.expectedValue, test.constValue, "Expected %d but got %d for %s", test.expectedValue, test.constValue, test.constName)
	}
}

func TestWaveformConstsUnchanged(t *testing.T) {
	type testCase struct {
		constValue    uint8
		expectedValue uint8
		constName     string
	}

	testCases := []testCase{
		{
			constValue:    WaveformSaw,
			expectedValue: 0,
			constName:     "WaveformSaw",
		},
		{
			constValue:    WaveformSine,
			expectedValue: 1,
			constName:     "WaveformSine",
		},
		{
			constValue:    WaveformHalfSine,
			expectedValue: 2,
			constName:     "WaveformHalfSine",
		},
		{
			constValue:    WaveformTriangle,
			expectedValue: 3,
			constName:     "WaveformTriangle",
		},
		{
			constValue:    WaveformPulse,
			expectedValue: 4,
			constName:     "WaveformPulse",
		},
		{
			constValue:    NoApply,
			expectedValue: 0,
			constName:     "NoApply",
		},
		{
			constValue:    Apply,
			expectedValue: 1,
			constName:     "Apply",
		},
		{
			constValue:    ApplyOnly,
			expectedValue: 2,
			constName:     "ApplyOnly",
		},
	}

	for _, test := range testCases {
		require.Equal(t, test.expectedValue, test.constValue, "Expected %d but got %d for %s", test.expectedValue, test.constValue, test.constName)
	}
}

func TestEffectConstantsUnchanged(t *testing.T) {
	type testCase struct {
		constValue    interface{}
		expectedValue interface{}
		constName     string
	}

	testCases := []testCase{
		{
			constValue:    MultiZoneEffectOff,
			expectedValue: uint8(0),
			constName:     "MultiZoneEffectOff",
		},
		{
			constValue:    MultiZoneEffectMove,
			expectedValue: uint8(1),
			constName:     "MultiZoneEffectMove",
		},
		{
			constValue:    DirectionRight,
			expectedValue: uint32(0),
			constName:     "DirectionRight",
		},
		{
			constValue:    DirectionLeft,
			expectedValue: uint32(1),
			constName:     "DirectionLeft",
		},
		{
			constValue:    TileEffectOff,
			expectedValue: uint8(0),
			constName:     "TileEffectOff",
		},
		{
			constValue:    TileEffectMorph,
			expectedValue: uint8(2),
			constName:     "TileEffectMorph",
		},
		{
			constValue:    TileEffectFlame,
			expectedValue: uint8(3),
			constName:     "TileEffectFlame",
		},
		{
			constValue:    HevCycleResultSuccess,
			expectedValue: uint8(0),
			constName:     "HevCycleResultSuccess",
		},
		{
			constValue:    HevCycleResultBusy,
			expectedValue: uint8(1),
			constName:     "HevCycleResultBusy",
		},
		{
			constValue:    HevCycleResultInterruptedByReset,
			expectedValue: uint8(2),
			constName:     "HevCycleResultInterruptedByReset",
		},
		{
			constValue:    HevCycleResultInterruptedByHomeKit,
			expectedValue: uint8(3),
			constName:     "HevCycleResultInterruptedByHomeKit",
		},
		{
			constValue:    HevCycleResultInterruptedByLAN,
			expectedValue: uint8(4),
			constName:     "HevCycleResultInterruptedByLAN",
		},
		{
			constValue:    HevCycleResultInterruptedByCloud,
			expectedValue: uint8(5),
			constName:     "HevCycleResultInterruptedByCloud",
		},
		{
			constValue:    HevCycleResultNone,
			expectedValue: uint8(255),
			constName:     "HevCycleResultNone",
		},
	}
	for _, test := range testCases {
		require.Equal(t, test.expectedValue, test.constValue, "Expected %d but got %d for %s", test.expectedValue, test.constValue, test.constName)
	}
}

func TestSetBroadcastAddress(t *testing.T) {
	require.Equal(t, net.IPv4bcast, conn.bcastAddress)
	SetBroadcastAddress(net.IPv4allsys)
	require.Equal(t, net.IPv4allsys, conn.bcastAddress)
	// reset
	SetBroadcastAddress(net.IPv4bcast)
}

func TestLookupDevices(t *testing.T) {
	wg := new(sync.WaitGroup)
	portChan := make(chan int)
	hostChan := make(chan string)
	wg.Add(1)
	expected := []byte{0x24, 0, 0, 0x34, 0x07, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0x02, 0, 0, 0}
	response := []byte{0x29, 0, 0, 0x14, 0x07, 0, 0, 0, 0xd0, 0x73, 0xd5, 0x2b, 0xa7, 0xb8, 0, 0, 0x4c,
		0x49, 0x46, 0x58, 0x56, 0x32, 0, 0, 0x40, 0x77, 0xb6, 0x2e, 0xb4, 0x5c, 0x02, 0, 0x03, 0, 0, 0,
		0x01, 0x7c, 0xdd, 0, 0}
	callCount := new(int)
	go CreateTestDeviceServer(t, hostChan, portChan, wg, expected, response, callCount)
	host := <-hostChan
	port := <-portChan
	wg.Wait()
	require.NotZero(t, port)
	config.Port = port
	ip := net.IP{}
	err := ip.UnmarshalText([]byte(host))
	require.NoError(t, err)
	conn.bcastAddress = ip
	devices, err := LookupDevices()
	require.NoError(t, err)
	require.Len(t, devices, 1)
	require.Equal(t, uint64(0xb8a72bd573d0), devices[0].hardwareAddress)
	require.False(t, devices[0].powerState)
	require.Equal(t, uint32(56700), devices[0].port)

	// reset variables
	conn.bcastAddress = net.IPv4bcast
	config.Port = defaultPort
}

func TestLookupDeviceByIP(t *testing.T) {
	t.Skip("Flaky, need to investigate")
	// Nil IP should return an error
	dev, err := LookupDeviceByIP(nil)
	require.Nil(t, dev)
	require.Error(t, err)

	wg := new(sync.WaitGroup)
	portChan := make(chan int)
	hostChan := make(chan string)
	wg.Add(1)
	expected := []byte{0x24, 0, 0, 0x34, 0x07, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0x02, 0, 0, 0}
	response := []byte{0x29, 0, 0, 0x14, 0x07, 0, 0, 0, 0xd0, 0x73, 0xd5, 0x2b, 0xa7, 0xb8, 0, 0, 0x4c,
		0x49, 0x46, 0x58, 0x56, 0x32, 0, 0, 0x40, 0x77, 0xb6, 0x2e, 0xb4, 0x5c, 0x02, 0, 0x03, 0, 0, 0,
		0x01, 0x7c, 0xdd, 0, 0}
	callCount := new(int)
	go CreateTestDeviceServer(t, hostChan, portChan, wg, expected, response, callCount)
	host := <-hostChan
	port := <-portChan
	wg.Wait()
	require.NotZero(t, port)
	config.Port = port
	ip, err := net.ResolveIPAddr("", host)
	require.NoError(t, err)
	dev, err = LookupDeviceByIP(ip)
	require.NoError(t, err)
	require.NotNil(t, dev)
	require.Equal(t, uint64(0xb8a72bd573d0), dev.hardwareAddress)
	require.False(t, dev.powerState)
	require.Equal(t, uint32(56700), dev.port)

	config.Port = defaultPort
}
