package golifx

import (
	"net"
	"strings"
	"time"
)

type connection struct {
	bcastAddress net.IP
}

func (c *connection) get() (*net.UDPConn, error) {
	if config.OutboundIP != nil {
		addr := net.UDPAddr{Port: 0, IP: net.ParseIP(*config.OutboundIP)}
		return net.ListenUDP("udp", &addr)
	}
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	h, _, err := net.SplitHostPort(conn.LocalAddr().String())
	if err != nil {
		return nil, err
	}
	addr := net.UDPAddr{Port: 0, IP: net.ParseIP(h)}
	return net.ListenUDP("udp", &addr)
}

// if expectedResponses is -1 then it will handle as many responses as possible (discovery), otherwise it'll probably be 1 to speed up the response when it's been found
func (c *connection) sendAndReceiveDead(inMessage *message, deadline time.Duration, addr net.Addr, expectedResponses int) ([]*message, error) {
	conn, err := c.get()
	if err != nil {
		return nil, err
	}
	err = conn.SetReadDeadline(time.Now().Add(deadline))
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	if err != nil {
		return nil, err
	}

	if addr != nil && !config.AlwaysBroadcast {
		host, _, err := net.SplitHostPort(addr.String())
		if err != nil {
			if strings.HasSuffix(err.Error(), "missing port in address") {
				host = addr.String()
			} else {
				return nil, err
			}
		}
		ip := net.IP{}
		_ = ip.UnmarshalText([]byte(host))
		_, _ = conn.WriteTo(inMessage.ReadRaw(), &net.UDPAddr{
			IP:   ip,
			Port: config.Port,
		})
	} else {
		_, err = conn.WriteTo(inMessage.ReadRaw(), &net.UDPAddr{
			IP:   c.bcastAddress,
			Port: config.Port,
		})
	}

	if err != nil {
		return nil, err
	}

	messages := make([]*message, 0)
	responses := 0
	for {
		buff := make([]byte, 4096)
		n, addr, err := conn.ReadFrom(buff)
		if n > 0 {
			responses++
		}
		if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
			return messages, nil
		}
		msg := makeMessage()
		msg.Write(buff[:n])
		msg.addr = addr

		messages = append(messages, msg)

		if expectedResponses > 0 && expectedResponses == responses {
			return messages, nil
		}
	}
}
