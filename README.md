# Go LIFX library

[![pipeline status](https://gitlab.com/wwsean08/golifx/badges/main/pipeline.svg)](https://gitlab.com/wwsean08/golifx/-/commits/main)
[![coverage report](https://gitlab.com/wwsean08/golifx/badges/main/coverage.svg)](https://gitlab.com/wwsean08/golifx/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/wwsean08/golifx)](https://goreportcard.com/report/gitlab.com/wwsean08/golifx)

Pure Go Implementation of LIFX protocol. This allows you to lookup LIFX devices, change color, power and receive info. This code has been developed and maintained by Ven at September 2015, and this fork is maintained by Sean Smith as of June 2020 in order to extend it to more devices.

![gopher](https://gitlab.com/wwsean08/golifx/-/raw/main/gopher.png)

## Installation

```bash
go get gitlab.com/wwsean08/golifx
```

## Example
```go
package main

import (
	"fmt"
	"gitlab.com/wwsean08/golifx"
	"time"
)

func main() {
	//Lookup all bulbs
	devices, _ := golifx.LookupDevices()
	//Get label
	location, _ := devices[0].GetLabel()

	fmt.Printf("Label: %s\n", location) //Ven LIFX

	//Get power state
	powerState, _ := devices[0].GetPowerState()

	//Turn if off
	if !powerState {
		devices[0].SetPowerState(true)
	}

	ticker := time.NewTicker(time.Second)
	counter := 0

	hsbk := &golifx.HSBK{
		Hue:        2000,
		Saturation: 13106,
		Brightness: 65535,
		Kelvin:     3200,
	}
	//Change color every second
	for _ = range ticker.C {
		devices[0].SetColorState(hsbk, 500)
		counter++
		hsbk.Hue += 5000
		if counter > 10 {
			ticker.Stop()
			break
		}
	}
	//Turn off
	devices[0].SetPowerState(false)
}


```
## Links
 - LIFX protocol specification http://lan.developer.lifx.com/
 - Community https://community.lifx.com/c/developing-with-lifx

## Licence
[WTFPL](http://www.wtfpl.net/)