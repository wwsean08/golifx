package golifx

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestDevice_JSONMarshallUnmarshall(t *testing.T) {
	dut := Device{
		hardwareAddress: 203028134458320,
		powerState:      true,
		port:            56700,
		infraredLevel:   65535,
		label:           "foo",
	}

	data, err := json.Marshal(dut)
	require.NoError(t, err)
	require.NotNil(t, data)
	require.NotZero(t, data)

	newDut := new(Device)
	err = json.Unmarshal(data, &newDut)
	require.NoError(t, err)
	require.Equal(t, dut.hardwareAddress, newDut.hardwareAddress)
	require.Equal(t, dut.powerState, newDut.powerState)
	require.Equal(t, dut.port, newDut.port)
	require.Equal(t, dut.infraredLevel, newDut.infraredLevel)
	require.Equal(t, dut.label, newDut.label)
}
